// const p = Promise.reject(new Error("reject reason..."));
// p.catch(err => console.log(err));

const p1 = new Promise(resolve => {
  setTimeout(() => {
    console.log("Async Operation 1 ...");
    resolve("name");
  }, 2000);
});

const p2 = new Promise(resolve => {
  setTimeout(() => {
    console.log("Async Operation 2 ...");
    resolve("Address");
  }, 2000);
});

Promise.race([p1, p2]).then(result => console.log(result));
