const express = require("express");
const router = express.Router();
const Joi = require("joi");

//Define JSON instead of database RECORDS
const products = [
  { id: 1, name: "Apple" },
  { id: 2, name: "Samsung" },
  { id: 3, name: "Huawei" },
  { id: 4, name: "Nokia" }
];

//HTTP GET REQUEST-----------------------------------------
router.get("/", (req, res) => {
  res.send(products);
});

//HTTP GET REQUEST-----------------------------------------
router.get("/:id", (req, res) => {
  const product = products.find(p => p.id === parseInt(req.params.id));
  if (!product) return res.status(404).send("product not found");
  res.send(product);
});

//HTTP POST REQUEST----------------------------------------
router.post("/", (req, res) => {
  const { error } = validateProduct(req.body); //just get error from result
  if (error) return res.status(400).send(error.details[0].message);
  const product = {
    id: products.length + 1,
    name: req.body.name
  };
  products.push(product);
  res.send(product);
});

//HTTP PUT REQUEST-----------------------------------------
router.put("/:id", (req, res) => {
  const product = products.find(p => p.id === parseInt(req.params.id));
  if (!product) return res.status(404).send("product not found");
  const { error } = validateProduct(req.body); //just get error from result
  if (error) return res.status(400).send(error.details[0].message);
  product.name = req.body.name;
  res.send(product);
});

//HTTP DELETE REQUEST---------------------------------------
router.delete("/:id", (req, res) => {
  const product = products.find(p => p.id === parseInt(req.params.id));
  if (!product) return res.status(404).send("product not found");
  const index = products.indexOf(product);
  products.splice(index, 1);
  res.send(product);
});

//####Function####
function validateProduct(product) {
  const schema = {
    name: Joi.string()
      .min(3)
      .required()
  };
  return Joi.validate(product, schema);
}

module.exports = router;
