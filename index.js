const config = require("config");
const debug = require("debug")("app:startup");
const morgan = require("morgan");
const helmet = require("helmet");
const express = require("express");
const logger = require("./middleware/logger");
const auth = require("./middleware/auth");
const app = express();
const products = require("./routes/products");
const homepage = require("./routes/homepage");

//view engine
app.set("view engine", "pug");
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(express.static("public"));
app.use(helmet());

app.use(logger);
app.use(auth);

app.use("/api/products", products);
app.use("/", homepage);

//configuration
console.log("Application Name:" + config.get("name"));
console.log("Mail Server:" + config.get("mail.host"));
//console.log("Mail Password:" + config.get("mail.password"));

//check which environment is my code Running
//Default environment of process with `app.get("env")` command is *development*
if (app.get("env") === "development") {
  app.use(morgan("tiny"));
  debug("Morgan is Running...");
}

//GET QUERY STRING
app.get("/api/products/:id/:number", (req, res) => {
  res.send(req.query);
});

//SET PORT****************************
const port = process.env.PORT || 3000;
app.listen(port, () => console.log(`listening to ${port}`));
